import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Test1WidgetComponent } from './test1-widget.component';

describe('Test1WidgetComponent', () => {
  let component: Test1WidgetComponent;
  let fixture: ComponentFixture<Test1WidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Test1WidgetComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Test1WidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
