import { Component, Input } from '@angular/core';
import { WidgetComponent } from '../widget-loader/widget-loader.component';

@Component({
  selector: 'app-test1-widget',
  templateUrl: './test1-widget.component.html',
  styleUrls: ['./test1-widget.component.scss']
})
export class Test1WidgetComponent implements WidgetComponent {
  @Input() data: any;
}
