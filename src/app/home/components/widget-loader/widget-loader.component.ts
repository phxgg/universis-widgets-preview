import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { WidgetItem } from 'src/app/utils/widget-item';
import { WidgetDirective } from 'src/app/directives/widget.directive';

export interface WidgetComponent {
  data: any;
}

@Component({
  selector: 'app-widget-loader',
  templateUrl: './widget-loader.component.html',
  styleUrls: ['./widget-loader.component.scss']
})
export class WidgetLoaderComponent implements OnInit {
  @Input() widget!: WidgetItem;
  @ViewChild(WidgetDirective, { static: true }) widgetHost!: WidgetDirective;

  ngOnInit(): void {
    console.log('[widget-loader]', this.widget);
    this.loadComponent();
  }

  loadComponent() {
    const viewContainerRef = this.widgetHost.viewContainerRef;
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent<WidgetComponent>(this.widget.component);
    componentRef.instance.data = this.widget.data;
  }
}
