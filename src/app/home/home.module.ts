import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './components/home/home.component';
import { RouterModule } from '@angular/router';
import { KtdGridModule } from '@katoid/angular-grid-layout';
import { UserProfileWidgetComponent } from './components/user-profile-widget/user-profile-widget.component';
import { CourseGradesWidgetComponent } from './components/course-grades-widget/course-grades-widget.component';
import { SafeHtmlPipe } from '../pipes/sanitizer.pipe';
import { WidgetLoaderComponent } from './components/widget-loader/widget-loader.component';
import { WidgetDirective } from '../directives/widget.directive';
import { WidgetService } from '../services/widget.service';
import { TranslateModule } from '@ngx-translate/core';
import { NgChartsModule } from 'ng2-charts';
import { Test1WidgetComponent } from './components/test1-widget/test1-widget.component';
import { Test2WidgetComponent } from './components/test2-widget/test2-widget.component';

@NgModule({
  declarations: [
    HomeComponent,
    WidgetLoaderComponent,
    UserProfileWidgetComponent,
    CourseGradesWidgetComponent,
    WidgetDirective,
    SafeHtmlPipe,
    Test1WidgetComponent,
    Test2WidgetComponent,
  ],
  providers: [WidgetService],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule.forChild(),
    KtdGridModule,
    HomeRoutingModule,
    NgChartsModule,
  ]
})
export class HomeModule { }
