import { Injectable } from "@angular/core";
import { WidgetItem } from "../utils/widget-item";
import { UserProfileWidgetComponent } from "../home/components/user-profile-widget/user-profile-widget.component";
import { CourseGradesWidgetComponent } from "../home/components/course-grades-widget/course-grades-widget.component";
import { Test1WidgetComponent } from "../home/components/test1-widget/test1-widget.component";
import { Test2WidgetComponent } from "../home/components/test2-widget/test2-widget.component";

@Injectable()
export class WidgetService {
  getWidgets() {
    return [
      new WidgetItem(
        UserProfileWidgetComponent,
        {},
      ),
      new WidgetItem(
        CourseGradesWidgetComponent,
        {},
      ),
      new WidgetItem(
        Test1WidgetComponent,
        {},
      ),
      new WidgetItem(
        Test2WidgetComponent,
        {},
      )
    ];
  }
}
