import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Test2WidgetComponent } from './test2-widget.component';

describe('Test2WidgetComponent', () => {
  let component: Test2WidgetComponent;
  let fixture: ComponentFixture<Test2WidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Test2WidgetComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Test2WidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
