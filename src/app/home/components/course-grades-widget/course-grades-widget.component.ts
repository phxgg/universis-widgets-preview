import { Component, Input, OnInit } from '@angular/core';
import { WidgetComponent } from '../widget-loader/widget-loader.component';
import { AngularDataContext } from '@themost/angular';
import { Observable, from } from 'rxjs';
import { ChartConfiguration, ChartType } from 'chart.js';

@Component({
  selector: 'app-course-grades-widget',
  templateUrl: './course-grades-widget.component.html',
  styleUrls: ['./course-grades-widget.component.scss']
})
export class CourseGradesWidgetComponent implements WidgetComponent, OnInit {
  @Input() data: any;
  public courseGrades$: Observable<any> = from(this.loadCourseGrades());

  public passedCourses: number = 28;
  public failedCourses?: number;
  public registeredCourses: number = 35;
  public doughnutChartData: number[] = [];
  public doughnutChartDatasets: ChartConfiguration<'doughnut'>['data']['datasets'] = [];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutColors = [
    {
      backgroundColor: [
        '#2500dd',
        '#BDB2F5',
      ]
    }
  ];
  public doughnutChartOptions = {};

  constructor(private context: AngularDataContext) { }

  ngOnInit(): void {
    this.failedCourses = this.registeredCourses - this.passedCourses;
    this.doughnutChartData = [this.passedCourses, this.failedCourses];
    this.doughnutChartDatasets = [{ data: this.doughnutChartData, label: "Courses" }];

    const text = this.passedCourses + '/' + this.registeredCourses;
    this.doughnutChartOptions = Object.assign({
      cutoutPercentage: 80,
      legend: {
        display: false
      },
      centerText: {
        display: true,
        text: text
      },
      tooltips: {
        position: 'nearest'
      }
    });


  }

  loadCourseGrades() {
    return new Promise((resolve, reject) => {
      resolve(true);
    })
    // return this.context.model('CourseGrades').asQueryable().where('Course/Code').equal(this.data.code).getItems();
  }
}
