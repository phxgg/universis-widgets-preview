import { Component, Input, OnInit } from '@angular/core';
import { WidgetComponent } from '../widget-loader/widget-loader.component';
import { AngularDataContext } from '@themost/angular';
import { Observable, from } from 'rxjs';

@Component({
  selector: 'app-user-profile-widget',
  templateUrl: './user-profile-widget.component.html',
  styleUrls: ['./user-profile-widget.component.scss']
})
export class UserProfileWidgetComponent implements WidgetComponent, OnInit {
  @Input() data: any;
  public user$: Observable<any> = from(this.loadUser());

  constructor(private context: AngularDataContext) { }

  ngOnInit(): void {
    this.user$.subscribe((user) => {
      console.log(user);
    })
  }

  loadUser() {
    return this.context.model('Users/Me').asQueryable().getItem();
  }
}
