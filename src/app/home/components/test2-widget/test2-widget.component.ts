import { Component, Input } from '@angular/core';
import { WidgetComponent } from '../widget-loader/widget-loader.component';

@Component({
  selector: 'app-test2-widget',
  templateUrl: './test2-widget.component.html',
  styleUrls: ['./test2-widget.component.scss']
})
export class Test2WidgetComponent implements WidgetComponent  {
  @Input() data: any;
}
