import { Component, OnInit } from '@angular/core';
import { KtdGridLayout, ktdTrackById } from '@katoid/angular-grid-layout';
import { WidgetItem } from 'src/app/utils/widget-item';
import { WidgetService } from 'src/app/services/widget.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public cols: number = 12;
  public rowHeight: number = 60;
  public layout: KtdGridLayout = [
    { id: 'UserProfileWidgetComponent', x: 0, y: 0, w: 3, h: 4 },
    { id: 'CourseGradesWidgetComponent', x: 3, y: 0, w: 3, h: 6 },
    { id: 'Test1WidgetComponent', x: 0, y: 0, w: 3, h: 3 },
    { id: 'Test2WidgetComponent', x: 3, y: 0, w: 3, h: 3 },
  ];
  public trackById = ktdTrackById;

  public components: WidgetItem[] = [];

  constructor(private widgetService: WidgetService) { }

  ngOnInit(): void {
    this.components = this.widgetService.getWidgets();
    console.log('home', this.components);
  }

  onLayoutUpdated(event: any) {
    console.log('layout updated', event);
  }

  findComponentById(id: string) {
    const component = this.components.find((component) => component.component.name === id);
    return component;
  }

}
