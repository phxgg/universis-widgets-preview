import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseGradesWidgetComponent } from './course-grades-widget.component';

describe('CourseGradesWidgetComponent', () => {
  let component: CourseGradesWidgetComponent;
  let fixture: ComponentFixture<CourseGradesWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourseGradesWidgetComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CourseGradesWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
